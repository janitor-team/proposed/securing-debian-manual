<?xml version="1.0"?>
<chapter id="intro"><title>Introduction</title>
<para>
One of the hardest things about writing security documents is that every case
is unique. Two things you have to pay attention to are the threat environment
and the security needs of the individual site, host, or network. For instance,
the security needs of a home user are completely different from a network in a
bank. While the primary threat a home user needs to face is the script kiddie
type of cracker, a bank network has to worry about directed attacks. 
Additionally, the bank has to protect their customer's data with arithmetic 
precision. In short, every user has to consider the trade-off between
usability and security/paranoia.</para>
<!-- Is this metaphor really appropriate? Sounds like rounding errors to me, 
era -->
<para>
Note that this manual only covers issues relating to software. 
The best software in the world can't protect you if someone can physically
access the machine. You can place it under your desk, or you can place
it in a hardened bunker with an army in front of it. Nevertheless the
desktop computer can be much more secure (from a software point of view)
than a physically protected one if the desktop is configured properly and
the software on the protected machine is full of security holes.
Obviously, you must consider both issues.</para>

<para>This document just gives an overview of what you can do 
to increase the security of your Debian GNU/Linux
system. If you have read other documents regarding Linux security, you
will find that there are common issues which might overlap with this
document. However, this document does not try to be the ultimate source
of information you will be using, it only tries to adapt this same
information so that it is meaningful to a Debian GNU/Linux system.
Different distributions do some things in different ways (startup of
daemons is one example); here, you will find material which is
appropriate for Debian's procedures and tools.</para>
<!-- 
# Does this approximate the intent of the original author? (FIXME: check)
# Original text said: "you will find here [sic] a different approach,
# using Debian's tools, regarding security." era
-->
<!-- IMHO yes, jfs -->

<section id="authors"><title>Authors</title>

    <para>The current maintainer of this document is Javier
Fernández-Sanguino Peña. Please forward him
any comments, additions or suggestions, and they will be
considered for inclusion in future releases of this manual.</para>

<para>This manual was started as a <emphasis>HOWTO</emphasis> by Alexander Reelsen. After it was published on the
    Internet, Javier Fernández-Sanguino Peña incorporated it into the <ulink url="http://www.debian.org/doc">Debian Documentation Project</ulink>. A number of
people have contributed to this manual (all contributions are listed
in the changelog) but the following deserve special mention since they
have provided significant contributions (full sections, chapters or
appendices):</para>

<itemizedlist>
<listitem><para>Stefano Canepa</para></listitem>
<listitem><para>Era Eriksson</para></listitem>
<listitem><para>Carlo Perassi</para></listitem>
<listitem><para>Alexandre Ratti</para></listitem>
<listitem><para>Jaime Robles</para></listitem>
<listitem><para>Yotam Rubin</para></listitem>
<listitem><para>Frederic Schutz</para></listitem>
<listitem><para>Pedro Zorzenon Neto</para></listitem>
<listitem><para>Oohara Yuuma</para></listitem>
<listitem><para>Davor Ocelic</para></listitem>
</itemizedlist>
</section>

<section id="guideformats"><title>Where to get the manual (and available formats)</title>
<para>
You can download or view the latest version of the Securing Debian
Manual from the <ulink url="http://www.debian.org/doc/manuals/securing-debian-howto/">Debian Documentation Project</ulink>. 
If you are reading a copy from another site, please check
the primary copy in case it provides new information. If you are reading
a translation, please review the version the translation refers to to
the latest version available. If you find that the version is behind
please consider using the original copy or review the to see what has changed.
</para>

<!-- Note to translators: adjust the en.txt to XX.txt where XX is your 
     language code -->
<para>If you want a full copy of the manual you can either download the <ulink url="http://www.debian.org/doc/manuals/securing-debian-howto/securing-debian-howto.en.txt">text version</ulink>
    or the <ulink url="http://www.debian.org/doc/manuals/securing-debian-howto/securing-debian-howto.en.pdf">PDF version</ulink> from the Debian Documentation Project's site. These versions might
be more useful if you intend to copy the document over to a portable device for
offline reading or you want to print it out. Be forewarned, the manual is over
two hundred pages long and some of the code fragments, due to the formatting
tools used, are not wrapped in the PDF version and might be printed incomplete.
</para>

<para>The document is also provided in text, html and PDF formats in the
    <ulink url="http://packages.debian.org/harden-doc">harden-doc</ulink> package.
Notice, however, that the package maybe not be completely up to date with the
document provided on the Debian site (but you can always use the source
package to build an updated version yourself).</para>

<para>This document is part of the documents distributed by the
    <ulink url="https://alioth.debian.org/projects/ddp/">Debian Documentation Project</ulink>. 
You can review the changes introduced in the document using
a web browser and obtaining information from the <ulink url="https://salsa.debian.org/ddp-team/securing-debian-manual">version control logs online</ulink>. You
can also checkout the code using Git with the following call in the command line:
<screen><computeroutput>$ </computeroutput><userinput>git clone https://salsa.debian.org/ddp-team/securing-debian-manual.git</userinput></screen></para>
</section>

<section><title>Organizational notes/feedback</title>
<para>
Now to the official part. At the moment I (Alexander Reelsen) wrote
most paragraphs of this manual, but in my opinion this should not stay
the case. I grew up and live with free software, it is part of my
everyday use and I guess yours, too. I encourage everybody to send me
feedback, hints, additions or any other suggestions you might have.
</para>
<para>
If you think, you can maintain a certain section or paragraph better,
then write to the document maintainer and you are welcome to do it.
Especially if you find a section marked as FIXME, that means the
authors did not have the time yet or the needed knowledge about the
topic. Drop them a mail immediately.</para>

<para>
The topic of this manual makes it quite clear that it is important to
keep it up to date, and you can do your part. Please contribute.
</para>
</section>

<section id="references"><title>Prior knowledge</title>
<para>
The installation of Debian GNU/Linux is not very difficult and you
should have been able to install it. If you already have some
knowledge about Linux or other Unices and you are a bit familiar with
basic security, it will be easier to understand this manual, as this
document cannot explain every little detail of a feature (otherwise
this would have been a book instead of a manual). If you are not that
familiar, however, you might want to take a look at
for where to find more in-depth information.
</para>
</section>

<section><title>Things that need to be written (FIXME/TODO)</title>

<para>This section describes all the things that need to be fixed in this
manual. Some paragraphs include <emphasis>FIXME</emphasis> or <emphasis>TODO</emphasis> tags
describing what content is missing (or what kind of work needs to be
done). The purpose of this section is to describe all the things that
could be included in the future in the manual, or enhancements that
need to be done (or would be interesting to add).</para>

<para>If you feel you can provide help in contributing content fixing any
element of this list (or the inline annotations), contact the main
author (<xref linkend="authors"/>).</para>


<itemizedlist>

<listitem><para>This document has yet to be updated based on the latest Debian releases.
The default configuration of some packages need to be adapted as they have been
modified since this document was written.</para></listitem>

<listitem><para>Expand the incident response information, maybe add some ideas
derived from Red Hat's Security Guide's
<ulink url="http://www.redhat.com/docs/manuals/linux/RHL-9-Manual/security-guide/ch-response.html">chapter on incident response</ulink>.</para></listitem>

<listitem><para>Write about remote monitoring tools (to check for system
availability) such as <application>monit</application>, <application>daemontools</application>
and <application>mon</application>. See <ulink url="http://linux.oreillynet.com/pub/a/linux/2002/05/09/sysadminguide.html">Sysamin Guide</ulink>.</para></listitem>

<listitem><para>Consider writing a section on how to build Debian-based network
appliances (with information such as the base system, <application>equivs</application> and FAI).</para></listitem>

<listitem><para>Check if 
        <ulink url="http://www.giac.org/practical/gsec/Chris_Koutras_GSEC.pdf">this site</ulink> has
relevant info not yet covered here.</para></listitem>

<listitem><para>Add information on how to set up a laptop with Debian,
        <ulink url="http://www.giac.org/practical/gcux/Stephanie_Thomas_GCUX.pdf">look here</ulink>.</para></listitem>

<listitem><para>Add information on how to set up a firewall using Debian
GNU/Linux. The section regarding firewalling is oriented currently
towards a single system (not protecting others...) also talk on how
to test the setup.</para></listitem>

<listitem><para>Add information on setting up a proxy firewall with Debian GNU/Linux 
stating specifically which packages provide proxy services (like 
<application>xfwp</application>, 
<application>ftp-proxy</application>, <application>redir</application>, 
<application>smtpd</application>,
<application>dnrd</application>, <application>jftpgw</application>, <application>oops</application>, 
<application>pdnsd</application>, <application>perdition</application>, 
<application>transproxy</application>, <application>tsocks</application>). Should point to the 
manual for any other info. Note that <application>zorp</application> is now 
available as a Debian package and <emphasis>is</emphasis> a proxy firewall 
(they also provide Debian packages upstream).</para></listitem>

<listitem><para>Information on service configuration with file-rc.</para></listitem>

<listitem><para>Check all the reference URLs and remove/fix those no longer available.</para></listitem>

<listitem><para>Add information on available replacements (in Debian) for common
servers which are useful for limited functionality. Examples: 
        <itemizedlist>
            <listitem><para>local lpr with cups (package)?</para></listitem>
            <listitem><para>remote lrp with lpr</para></listitem>
            <listitem><para>bind with dnrd/maradns</para></listitem>
            <listitem><para>apache with dhttpd/thttpd/wn (tux?)</para></listitem>
            <listitem><para>exim/sendmail with ssmtpd/smtpd/postfix</para></listitem>
            <listitem><para>squid with tinyproxy</para></listitem>
            <listitem><para>ftpd with oftpd/vsftp</para></listitem>
            <listitem><para>...</para></listitem>
        </itemizedlist>
</para></listitem>
<listitem><para>More information regarding security-related kernel patches in
Debian, including the ones shown above and
specific information on how to enable these patches in a Debian system.
        <itemizedlist>
            <listitem><para>Linux Intrusion Detection (<application>kernel-patch-2.4-lids</application>)</para></listitem>
            <listitem><para>Linux Trustees (in package <application>trustees</application>)</para></listitem>
            <listitem><para><ulink url="http://wiki.debian.org/SELinux">NSA Enhanced Linux</ulink></para></listitem>
            <listitem><para><application>linux-patch-openswan</application></para></listitem>
            <listitem><para>...</para></listitem>
        </itemizedlist>
</para></listitem>

<listitem><para>Details of turning off unnecessary network services (besides 
<command>inetd</command>), it is partly in the hardening procedure but could be 
broadened a bit.</para></listitem>

<listitem><para>Information regarding password rotation which is closely related
to policy.</para></listitem>

<listitem><para>Policy, and educating users about policy.</para></listitem>

<listitem><para>More about tcpwrappers, and wrappers in general?</para></listitem>

<listitem><para><filename>hosts.equiv</filename> and other major security holes.</para></listitem>

<listitem><para>Issues with file sharing servers such as Samba and NFS? </para></listitem>

<listitem><para>suidmanager/dpkg-statoverrides.</para></listitem>

<listitem><para>lpr and lprng.</para></listitem>

<listitem><para>Switching off the GNOME IP things.</para></listitem>

<listitem><para>Talk about pam_chroot (see <ulink url="http://lists.debian.org/debian-security/2002/debian-security-200205/msg00011.html"/>)
and its usefulness to limit users. Introduce information related to
<ulink type="block" url="http://online.securityfocus.com/infocus/1575"/>.
<application>pdmenu</application>, for example is available in Debian (whereas flash is not).</para></listitem>

<listitem><para>Talk about chrooting services, some more info on
        <ulink url="http://www.linuxfocus.org/English/January2002/article225.shtml">this Linux Focus article</ulink>.</para></listitem>
<!-- no longer available
<url id="http://www.nuclearelephant.com/papers/chroot.html">,  and
<url id="http://www.linuxsecurity.com/feature_stories/feature_story-99.html">.
                             -->

<listitem><para>Talk about programs to make chroot jails. <application>compartment</application>
and <application>chrootuid</application> are waiting in incoming. Some others
(makejail, jailer) could also be introduced.</para></listitem>

<!-- FIXME: No longer available in that URL
<listitem><para>Add information provided by Karl Hegbloom regarding chrooting 
Bind 9, see <url id="http://people.pdxlinux.org/~karlheg/Secure_Bind9_uHOWTO/Secure_Bind_9_uHOWTO.xhtml">.
-->

<!-- No longer relevant
<listitem><para>Add information provided by Pedro Zornenon to chrooting Bind 8 only for 
potato though :(, see 
<url id="http://people.debian.org/~pzn/howto/chroot-bind.sh.txt"> (include the 
whole script?).
-->

<listitem><para>More information regarding log analysis software (i.e. logcheck
and logcolorise).</para></listitem>

<listitem><para>'advanced' routing (traffic policing is security related).</para></listitem>

<listitem><para>limiting <command>ssh</command> access to running certain commands.</para></listitem>

<listitem><para>using dpkg-statoverride.</para></listitem>

<listitem><para>secure ways to share a CD burner among users.</para></listitem>

<listitem><para>secure ways of providing networked sound in addition to network
display capabilities (so that X clients' sounds are played on the X
server's sound hardware).</para></listitem>

<listitem><para>securing web browsers.</para></listitem>

<listitem><para>setting up ftp over <command>ssh</command>.</para></listitem>

<listitem><para>using crypto loopback file systems.</para></listitem>

<listitem><para>ncrypting the entire file system.</para></listitem>

<listitem><para>steganographic tools.</para></listitem>

<listitem><para>setting up a PKA for an organization.</para></listitem>

<listitem><para>using LDAP to manage users. There is a HOWTO of ldap+kerberos
for Debian at <ulink url="http://www.bayour.com"/> written by Turbo Fredrikson.</para></listitem>

<listitem><para>How to remove information of reduced utility in production systems
such as <filename>/usr/share/doc</filename>, <filename>/usr/share/man</filename> (yes, security by obscurity).</para></listitem>

<listitem><para>More information on lcap based on the packages README file
(well, not there yet, see
<ulink url="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=169465" name="Bug #169465"/>) and from the article from LWN:
<ulink url="http://lwn.net/1999/1202/kernel.php3" name="Kernel development"/>.</para></listitem>

<listitem><para>Add Colin's article on how to setup a chroot environment for a full 
sid system (<ulink url="http://people.debian.org/~walters/chroot.html"/>).</para></listitem>

<listitem><para>Add information on running multiple <command>snort</command> sensors in a given
system (check bug reports sent to <application>snort</application>).</para></listitem>

<listitem><para>Add information on setting up a honeypot (<application>honeyd</application>).</para></listitem>

<listitem><para>Describe situation wrt to FreeSwan (orphaned) and OpenSwan.
VPN section needs to be rewritten.</para></listitem>

<listitem><para>Add a specific section about databases, current installation defaults
and how to secure access.</para></listitem>

<listitem><para>Add a section about the usefulness of virtual servers (Xen et al).</para></listitem>

<listitem><para>Explain how to use some integrity checkers (AIDE, integrit or samhain).
The basics are simple and could even explain some configuration improvements.</para></listitem>
</itemizedlist>
</section>

<section id="credits"><title>Credits and thanks!</title>
<itemizedlist>
<listitem><para>Alexander Reelsen wrote the original document. </para></listitem>

<listitem><para> added more info to the original doc.</para></listitem>

<listitem><para>Robert van der Meulen provided the quota paragraphs and many good ideas.</para></listitem>

<listitem><para>Ethan Benson corrected the PAM paragraph and had some good ideas.</para></listitem>

<listitem><para>Dariusz Puchalak contributed some information to several chapters.</para></listitem>

<listitem><para>Gaby Schilders contributed a nice Genius/Paranoia idea.</para></listitem>

<listitem><para>Era Eriksson smoothed out the language in a lot of
places and contributed the checklist appendix.</para></listitem>

<listitem><para>Philipe Gaspar wrote the LKM information.</para></listitem>

<listitem><para>Yotam Rubin contributed fixes for many typos as well
as information regarding bind versions and MD5 passwords.</para></listitem>

<listitem><para>Francois Bayart provided the appendix describing how to set up a bridge
firewall.</para></listitem>

<listitem><para>Joey Hess wrote the section describing how Secure Apt works on the <ulink url="http://wiki.debian.org/SecureApt">Debian Wiki</ulink>.</para></listitem>

<listitem><para>Martin F. Krafft wrote some information on his blog regarding fingerprint
verification which was also reused for the Secure Apt section.</para></listitem>

<listitem><para>Francesco Poli did an extensive review of the manual and provided quite a
lot of bug reports and typo fixes which improved and helped update  the
document.</para></listitem>

<listitem><para>All the people who made suggestions for improvements that
(eventually) were included here (see <xref linkend="guideformats"/>).</para></listitem>

<listitem><para>(Alexander) All the folks who encouraged me to write this HOWTO
(which was later turned into a manual).</para></listitem>

<listitem><para>The whole Debian project.</para></listitem>
</itemizedlist>
</section>
</chapter>
